package com.company;

import java.sql.*;
import java.util.Scanner;

public class JDBCTerminal {
    String url = "jdbc:mysql://localhost:3306/University";
    String user = "root";
    String pass = "";
    Connection connection;
    Statement statement;

    public JDBCTerminal() {
        connect();
    }

    public JDBCTerminal(String url, String user, String pass) {
        this.url = url;
        this.user = user;
        this.pass = pass;
        connect();
    }

    private void connect() {
        try {
            connection = DriverManager.getConnection(url, user, pass);
            statement = connection.createStatement();
            System.out.println("Connection created successfully");
        } catch (SQLException e) {
            System.out.println("Connection failed");
            e.printStackTrace();
        }
    }

    private void runSamples() {
        System.out.println("Running examples:");
        String q1 = "select * from instructor where dept_name = 'Comp. Sci.'";
        System.out.println("Query:\n" + q1);
        try {
            ResultSet resultSet = statement.executeQuery(q1);
            showTable(resultSet);
        } catch (SQLException e) {
            System.out.println("An error occur executing: " + q1);
            e.printStackTrace();
        }
        String q2 = "update instructor set salary = salary * 1.05";
        System.out.println("Query:\n" + q2);
        try {
            statement.execute(q2);
            System.out.println("Database updated");
        } catch (SQLException e) {
            System.out.println("An error occur executing: " + q1);
            e.printStackTrace();
        }
        System.out.println();
        System.out.println();
        String q3 = "select * from instructor";
        System.out.println("Query:\n" + q3);
        try {
            ResultSet resultSet = statement.executeQuery(q3);
            showTable(resultSet);
        } catch (SQLException e) {
            System.out.println("An error occur executing: " + q3);
            e.printStackTrace();
        }
        System.out.println();
    }

    public void run() {
        //runSamples();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your query or you can type \"exit\" to terminate the program:");
        String query = scanner.nextLine();
        while (!query.equals("exit")) {
            if (query.equals("samples")) {
                runSamples();
            } else {
                try {
                    statement.execute(query);
                    ResultSet resultSet = statement.getResultSet();
                    if (resultSet == null)
                        System.out.println("Database updated");
                    else
                        showTable(resultSet);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            query = scanner.nextLine();
        }
    }

    private void showTable(ResultSet set) throws SQLException {
        ResultSetMetaData metaData = set.getMetaData();
        int columnCount = metaData.getColumnCount();
        int rowSize = 0;
        System.out.println();
        for (int i = 1; i <= columnCount; i++) {
            System.out.printf("|%-" + metaData.getColumnDisplaySize(i) + "s", metaData.getColumnName(i));
            rowSize += metaData.getColumnDisplaySize(i) + 1;
        }
        System.out.println("|");
        for (int i = 0; i < rowSize; i++)
            System.out.print("-");
        System.out.println("-");
        int rowCount = 0;
        while (set.next()) {
            for (int i = 1; i <= columnCount; i++)
                System.out.printf("|%-" + metaData.getColumnDisplaySize(i) + "s", set.getString(metaData.getColumnName(i)));
            System.out.println("|");
            rowCount++;
        }
        System.out.println(rowCount + " rows in set");
    }
}
